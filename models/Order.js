const mongoose = require('mongoose')

const OrderSchema = new mongoose.Schema({

 	 purchasedOn: {
 	 	type: Date,
 	 	default: new Date()
 	 },
 	 totalAmount: {
 	 	type: Number,
        default: 0
 	 },
     user: [
        {
            userId: {
                type: String,
                required: [true, "User Id is required."]
            }
        }
     ],
 	 products: [

 	 	{
 	 		productId: {
                type: String,
                required: [true, "Product Id is required."]
            },
            price: {
                type: Number,
                required: [true, "Product Price is required."]
            },
            productQty: {
                type: Number,
                required: [true, "Product Quantity is required."]
            },
            subtotal: {
                type: Number,
                required: [true, "Subtotal is required."]
            }
 	 	}

 	]
 	
})

module.exports = mongoose.model('Order', OrderSchema)