const mongoose = require('mongoose')
const Order = require('../models/Order')

const UserSchema = new mongoose.Schema({

 	firstName: {
 		type: String,
 		required: [true, "First name is required."]
 	},
    lastName: {
        type: String,
        required: [true, "Last name is required."]
    },
 	email: {
 		type: String,
 		required: [true, "Email is required."]
 	},
 	password: {
 		type: String,
 		required: [true, "Password is required."]
 	},
    confirmPassword: {
        type: String,
        required: [true, "Please confirm your password."]
    },
 	isAdmin: {
 		type: Boolean,
 		default: false
 	},
 	mobileNo: {
 		type: String,
 		required: [true, "Mobile Number is required."]
 	},
    dateRegistered: {
        type: Date,
        default: new Date()
    },
 	orders: [

        {
            // orderId: {
            //     type: String,
            //     required: [true, "Order Id is required."]
            // },
            orderedOn: {
                type: Date,
                default: new Date()
            }
        }

    ]
 	
})

module.exports = mongoose.model('User', UserSchema)