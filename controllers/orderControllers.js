const Order = require('../models/Order')
const Product = require('../models/Product')
const User = require('../models/User')
const bcrypt = require('bcrypt')

const {createAccessToken} = require("../auth.js")

module.exports.getOrders = (req,res) => {

	Order.find()
	.then(orders => {
		res.send(orders)
	})
	.catch (err => {
		res.send(err)
	})
}

module.exports.createOrder = (req,res) => {

	if (req.user.isAdmin === true) {
		res.send({auth: "You are not allowed to make this action."})
	} else {

		const foundProductId = req.body;
		const productIds = foundProductId.map(item => {
			return item
		})

		const newOrder = new Order({
			totalAmount: productIds.map(o => o.subtotal).reduce((a, c) => { return a + c }),
			user:[],
			product: []
		})

		console.log(productIds)

		User.findById(req.user.id)
		.then (foundUser => {
			newOrder.user.push({userId: req.user.id})
			console.log(newOrder)

			return foundUser.save()
		})
		.then (user => {
			console.log(user)
			return productIds
		})
		.then (products => {
			console.log(products)
			newOrder.products = newOrder.products.concat(productIds)

			return newOrder.save()
		})
		.then (userOrder => {
			console.log(userOrder)

			return User.findById(req.user.id)
		})
		.then (user => {
			console.log(newOrder)
			user.orders.push(newOrder)
			console.log(user);
			return user.save()
		})
		.then (order => {
			res.send(newOrder)
		})

	}
}