const Product = require('../models/Product')


// CREATE PRODUCT
module.exports.createProduct = (req,res) => {

	const product = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
	})

	product.save()
	.then(products=> {
		res.send(products)
	})
	.catch(err=>{
		res.send(err)
	})
}

// GET ALL PRODUCTS (ADMIN ONLY)
module.exports.getAllProducts = (req, res) => {

	Product.find({},{_id:0})
	.then(allProducts => {
		res.send(allProducts)
	})
	.catch(err=>{
		res.send(err)
	})

}


// GET ALL AVAILABLE PRODUCTS
module.exports.allAvailable = (req,res) => {

	Product.find({isAvailable: true},{_id: 0})
	.then(availProduct => {
		res.send(availProduct)
	})
	.catch (err => {
		res.send(err)
	})

}


// GET SINGLE PRODUCT
module.exports.singleProduct = (req, res) =>{

	Product.findById(req.params.id,{_id:0})
	.then (foundProduct => {
		res.send(foundProduct)
	})
	.catch(error => {
		res.send(error)
	})

}


// UPDATE PRODUCT
module.exports.updateProduct = (req,res) => {

	let updatedProduct = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.id, updatedProduct, {new:true})
	.then(updatedProduct => {
		res.send(updatedProduct)
	})
	.catch(err =>{
		res.send(err)
	})

}

// ARCHIVE PRODUCT
module.exports.archive = (req,res) => {

	let archive = {
		isAvailable: false
	}

	Product.findByIdAndUpdate(req.params.id, archive ,{new:true})
	.then(archivedProduct => {
		res.send(archivedProduct)
	})
	.catch(err => {
		res.send(err)
	})
}

// MAKE PRODUCT ACTIVE
module.exports.makeActive = (req,res) => {

	let active = {
		isAvailable: true
	}

	Product.findByIdAndUpdate(req.params.id, active ,{new:true})
	.then(activeProduct => {
		res.send(activeProduct)
	})
	.catch(err => {
		res.send(err)
	})
}