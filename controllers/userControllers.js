const User = require('../models/User')
const bcrypt = require('bcrypt')

const {createAccessToken} = require("../auth.js")

// USER REGISTRATION
module.exports.registerUser = (req, res) => {
	const hashedPw = bcrypt.hashSync(req.body.password, 10)
	const hashedConfirm = bcrypt.hashSync(req.body.confirmPassword, 10)
	console.log(hashedPw)

	const exists = User.find({email: req.body.email})
	console.log(exists)
	// if (exists){
	// 	res.send("This email already exists.")
	// } else 
	if (req.body.password.length < 8) {
		res.send("Password needs to be at least 8 characters long.")
	} else if (req.body.password !== req.body.confirmPassword) {
		res.send("Passwords don't match.")
	} else {
		
		const user = new User({
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			email: req.body.email,
			password: hashedPw,
			confirmPassword: hashedConfirm,
			mobileNo: req.body.mobileNo
		})

		user.save()
		.then(user=>{
			res.send(user)
		})
		.catch (err=>{
			res.send(err)
		})
	}
}

// USER LOGIN
module.exports.login = (req, res)=> {

	User.findOne({email: req.body.email})
	.then(foundUser => {
		if (foundUser === null) {
			res.send("User does not exist.")
		} else {

			const correctPw = bcrypt.compareSync(req.body.password, foundUser.password)

			if (correctPw) {
				res.send({accessToken: createAccessToken(foundUser)})
			} else {
				res.send("Email and/or password is incorrect.")
			}
		}

	})
	.catch(error =>{
		res.send(error)
	})
 
} 


// SET USER TO ADMIN
module.exports.makeAdmin = (req, res) => {

	let updateAdmin = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updateAdmin, {new:true})
	.then (updatedUser => {
		console.log(updatedUser.password)
		res.send(updatedUser)
	})
	.catch (err=>{
		res.send(err)
	})

}

// GET ALL USERS (ADMIN ONLY)
module.exports.allUsers = (req,res) => {

	User.find({},{_id: 0,password: 0, confirmPassword: 0})
	.then(foundUsers => {
		res.send(foundUsers)
	})

}

// RETRIEVE USERS ORDER
module.exports.userOrder = (req,res) => {

	User.findById(req.user.id)
	.then (foundId => {
		res.send(foundId.orders)

	}) 

} 

// UPDATE USER INFO
module.exports.updateUser = (req,res) => {

	let update = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	if (req.user.isAdmin === true) {
		res.send({auth: "Authentication Failed."})
	} else {
		User.findByIdAndUpdate(req.user.id, update, {new:true})
		.then (updatedUser => {
			res.send(updatedUser)
		})
		.catch (err=> {
			res.send(err)
		})
	}

}
