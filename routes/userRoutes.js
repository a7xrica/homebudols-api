const express = require('express')
const router = express.Router()

const {registerUser, login, makeAdmin, updateUser, userOrder, allUsers} = require('../controllers/userControllers')
const {verify, verifyAdmin} = require('../auth')

router.post('/', registerUser)

router.get('/', verify, verifyAdmin, allUsers)

router.post('/login', login)

router.put('/userUpdate', verify, updateUser)

router.get('/userOrder', verify, userOrder)

router.put('/makeAdmin/:id', verify, verifyAdmin, makeAdmin)

module.exports = router 