const express = require('express')
const router = express.Router()

const {allAvailable, singleProduct, createProduct, updateProduct, archive, getAllProducts, makeActive} = require('../controllers/productControllers')
const {verify, verifyAdmin} = require('../auth')

router.post('/', verify, verifyAdmin, createProduct)

router.get('/', allAvailable)

router.get('/allProducts', verify, verifyAdmin, getAllProducts)

router.put('/archiveProduct/:id', verify, verifyAdmin, archive)

router.get('/:id', singleProduct)

router.put('/:id', verify, verifyAdmin, updateProduct)

router.put('/activate/:id', verify, verifyAdmin, makeActive)

module.exports = router 