const express = require('express')
const router = express.Router()

const {getOrders, createOrder} = require('../controllers/orderControllers')
const {verify, verifyAdmin} = require('../auth')

router.get('/', verify, verifyAdmin, getOrders)

router.post('/', verify, createOrder)


module.exports = router 