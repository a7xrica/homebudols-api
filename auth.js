const jwt = require("jsonwebtoken")
const secret = "HomeBudolsAPI"

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data,secret,{})
}


module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization
	console.log(req.headers)

	if (typeof token === "undefined"){
		res.send({auth: "Authentication Failed"})
	} else {
		token = token.slice(7,token.length)

		jwt.verify(token,secret,function(err,decoded){
			console.log(decoded)
			if (err){
				res.send({auth: "Authentication Failed"})
			} else {
				console.log(decoded)
				req.user = decoded
				next()
			}
		})
	}
}

module.exports.verifyAdmin = (req,res,next) => {

	if (req.user.isAdmin){
		next()
	} else {
		res.send({auth: "You are not allowed to make this action."})
	}

}

