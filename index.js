const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

const app = express()  
const port = process.env.PORT || 4000

// Database Connection
mongoose.connect('mongodb+srv://a7xrica:mayella16@cluster0.urw1w.mongodb.net/homebudols-api?retryWrites=true&w=majority', {

	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false

})
 
.then(()=> {
	console.log("Connected to Home Budols Database.")
})

.catch((err)=>{
	console.log(err.message)
})

app.use(cors())

app.use(express.json()) 


// Users
const userRoutes = require('./routes/userRoutes')
app.use('/api/users', userRoutes)

// Products
const productRoutes = require('./routes/productRoutes')
app.use('/api/products', productRoutes)

// Orders
const orderRoutes = require('./routes/orderRoutes')
app.use('/api/orders', orderRoutes)


app.listen(port, () => console.log(`Server running at Localhost: ${port}`))